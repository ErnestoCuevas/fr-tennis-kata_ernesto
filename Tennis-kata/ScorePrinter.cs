﻿using System;
using System.Runtime.CompilerServices;

namespace Tennis_kata
{
    public class ScorePrinter
    {
        private enum Winner { None, FirstPlayer, SecondPlayer };


        public String PrintScore(String firstPlayerName, Int32 firstPlayerPoints, String secondPlayerName, Int32 secondPlayerPoints)
        {
            String result = null;

            switch (GetWinner(firstPlayerPoints, secondPlayerPoints))
            {
                case Winner.FirstPlayer:
                    result = "Game for " + firstPlayerName;
                    break;
                case Winner.SecondPlayer:
                    result = "Game for " + secondPlayerName;
                    break;
                default:
                    result = GetGameInPlayScore(firstPlayerName, firstPlayerPoints, secondPlayerName, secondPlayerPoints);
                    break;
            }

            return result;
        }

        private String GetGameInPlayScore(String firstPlayerName, Int32 firstPlayerPoints, String secondPlayerName, Int32 secondPlayerPoints)
        {
            if (IsStandardPunctuation(firstPlayerPoints, secondPlayerPoints))
            {
                return firstPlayerName + " " + firstPlayerPoints.ToTennisName() + ", " + secondPlayerName + " " + secondPlayerPoints.ToTennisName();
            }
            if (firstPlayerPoints == secondPlayerPoints)
            {
                return "Deuce";
            }
            if (firstPlayerPoints > secondPlayerPoints)
            {
                return "Advantage " + firstPlayerName;
            }
            if (secondPlayerPoints > firstPlayerPoints)
            {
                return "Advantage " + secondPlayerName;
            }
            return null;
        }

        private Boolean IsStandardPunctuation(Int32 firstPlayerPoints, Int32 secondPlayerPoints)
        {
            if (firstPlayerPoints < 4 || secondPlayerPoints < 4)
            {
                return true;
            }

            return false;
        }

        private Winner GetWinner(Int32 firstPlayerPoints, Int32 secondPlayerPoints)
        {
            if ((firstPlayerPoints < 4 && secondPlayerPoints < 4) || Math.Abs(firstPlayerPoints - secondPlayerPoints) < 2)
            {
                return Winner.None;
            }

            if (firstPlayerPoints > secondPlayerPoints)
            {
                return Winner.FirstPlayer;
            }

            return Winner.SecondPlayer;

        }

    }
}
