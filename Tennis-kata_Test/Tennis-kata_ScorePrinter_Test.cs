﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tennis_kata;

namespace Tennis_kata_Test
{
    [TestClass]
    public class TennisKataScorePrinterTest
    {
        private ScorePrinter _printer = null;
        private String _firstPlayerName = "Andre Agassi";
        private String _secondPlayerName = "John McEnroe";

        [TestInitialize]
        public void InitializePrinter()
        {
            _printer = new ScorePrinter();
        }

        [TestCleanup]
        public void DereferencePrinter()
        {
            _printer = null;
        }

        [TestMethod]
        public void TestGameFinished()
        {
            Assert.AreEqual<String>("Game for " + _firstPlayerName, PrintScore(5, 3));
            Assert.AreEqual<String>("Game for " + _firstPlayerName, PrintScore(4, 2));
            Assert.AreNotEqual<String>("Game for " + _firstPlayerName, PrintScore(5, 4));
            Assert.AreNotEqual<String>("Game for " + _firstPlayerName, PrintScore(4, 3));
            Assert.AreNotEqual<String>("Game for " + _firstPlayerName, PrintScore(3, 1));

            Assert.AreEqual<String>("Game for " + _secondPlayerName, PrintScore(3, 5));
            Assert.AreEqual<String>("Game for " + _secondPlayerName, PrintScore(2, 4));
            Assert.AreNotEqual<String>("Game for " + _secondPlayerName, PrintScore(4, 5));
            Assert.AreNotEqual<String>("Game for " + _secondPlayerName, PrintScore(3, 4));
            Assert.AreNotEqual<String>("Game for " + _secondPlayerName, PrintScore(1, 3));
        }

        [TestMethod]
        public void TestGamePoints()
        {
            Assert.AreEqual<String>(_firstPlayerName + " love, " + _secondPlayerName + " love", PrintScore(0, 0));
            Assert.AreEqual<String>(_firstPlayerName + " fifteen, " + _secondPlayerName + " love", PrintScore(1, 0));
            Assert.AreEqual<String>(_firstPlayerName + " thirty, " + _secondPlayerName + " love", PrintScore(2, 0));
            Assert.AreEqual<String>(_firstPlayerName + " forty, " + _secondPlayerName + " love", PrintScore(3, 0));
            Assert.AreEqual<String>(_firstPlayerName + " fifteen, " + _secondPlayerName + " fifteen", PrintScore(1, 1));
            Assert.AreEqual<String>(_firstPlayerName + " thirty, " + _secondPlayerName + " fifteen", PrintScore(2, 1));
            Assert.AreEqual<String>(_firstPlayerName + " forty, " + _secondPlayerName + " fifteen", PrintScore(3, 1));
            Assert.AreEqual<String>(_firstPlayerName + " thirty, " + _secondPlayerName + " thirty", PrintScore(2, 2));
            Assert.AreEqual<String>(_firstPlayerName + " forty, " + _secondPlayerName + " thirty", PrintScore(3, 2));
            Assert.AreEqual<String>(_firstPlayerName + " forty, " + _secondPlayerName + " forty", PrintScore(3, 3));

            Assert.AreEqual<String>(_firstPlayerName + " love, " + _secondPlayerName + " love", PrintScore(0, 0));
            Assert.AreEqual<String>(_firstPlayerName + " love, " + _secondPlayerName + " fifteen", PrintScore(0, 1));
            Assert.AreEqual<String>(_firstPlayerName + " love, " + _secondPlayerName + " thirty", PrintScore(0, 2));
            Assert.AreEqual<String>(_firstPlayerName + " love, " + _secondPlayerName + " forty", PrintScore(0, 3));
            Assert.AreEqual<String>(_firstPlayerName + " fifteen, " + _secondPlayerName + " fifteen", PrintScore(1, 1));
            Assert.AreEqual<String>(_firstPlayerName + " fifteen, " + _secondPlayerName + " thirty", PrintScore(1, 2));
            Assert.AreEqual<String>(_firstPlayerName + " fifteen, " + _secondPlayerName + " forty", PrintScore(1, 3));
            Assert.AreEqual<String>(_firstPlayerName + " thirty, " + _secondPlayerName + " thirty", PrintScore(2, 2));
            Assert.AreEqual<String>(_firstPlayerName + " thirty, " + _secondPlayerName + " forty", PrintScore(2, 3));
            Assert.AreEqual<String>(_firstPlayerName + " forty, " + _secondPlayerName + " forty", PrintScore(3, 3));
        }

        [TestMethod]
        public void TestDeuce()
        {
            Assert.AreEqual<String>("Deuce", PrintScore(4, 4));
            Assert.AreEqual<String>("Deuce", PrintScore(5, 5));
        }

        [TestMethod]
        public void TestAdvantage()
        {
            Assert.AreEqual<String>("Advantage " + _firstPlayerName, PrintScore(5, 4));
            Assert.AreEqual<String>("Advantage " + _secondPlayerName, PrintScore(4, 5));
        }

        private String PrintScore(Int32 firstPlayerPoints, Int32 secondPlayerPoints)
        {
            String result = _printer.PrintScore(_firstPlayerName, firstPlayerPoints, _secondPlayerName, secondPlayerPoints);
            return result;
        }

    }
}
